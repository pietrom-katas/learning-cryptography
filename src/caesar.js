function caesar(text, key) {
    return text
        .toLowerCase()
        .split('')
        .reduce((acc, curr) => acc + crypt(curr, key), "")
}

const zero = "a".charCodeAt(0)
const encryptable = /[a-z]/

function crypt(char, key) {
    return !char.match(encryptable) ? char : addKey(char, key)
}

function addKey(char, key) {
    return String.fromCharCode((char.charCodeAt(0) - zero + key) % 26 + zero)
}

module.exports = caesar