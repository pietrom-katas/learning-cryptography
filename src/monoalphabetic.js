const zero = "a".charCodeAt(0)
const encryptable = /[a-z]/
const operations = {
  encrypt: (char, key) => !char.match(encryptable) ? char : key[char.charCodeAt(0) - zero],
  decrypt: (char, key) => {
    const index = key.indexOf(char)
    return index < 0 ? char : String.fromCharCode(index + zero)
  }
}

function monoalphabetic(text, mode, key) {
  const transform = operations[mode]
  return text
        .toLowerCase()
        .split('')
        .reduce((acc, curr) => acc + transform(curr, key), "")
}

module.exports = { 
  monoalphabetic,
  operations: Object.keys(operations)
}
