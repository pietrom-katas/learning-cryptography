const mono = require('../src/monoalphabetic')

const plainText = `
Libertà va cercando, ch'è sì cara
come sa chi per le vita rifiuta

Così preziosa come il vino, così gratis come la tristezza...

E il naufragar m'è dolce in questo mare.
`

const identityKey = 'abcdefghijklmnopqrstuvwxyz'

const key = 'qwertyuioplkjhgfdsazxcvbnm'

describe(`Caesar's cipher`, () => {
    test('plain texts remains unchanged with identity substitution', () => {
        expect(mono(plainText, 'encrypt', identityKey)).toBe(plainText.toLowerCase());
    })

    test('can encrypt and decript using the same key', () => {
        const encrypted = mono(plainText, 'encrypt', key)
        const decrypted = mono(encrypted, 'decrypt', key)
        expect(decrypted).toBe(plainText.toLowerCase())
    })
})
