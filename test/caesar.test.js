const caesar = require('../src/caesar')

const plainText = `
Libertà va cercando, ch'è sì cara
come sa chi per le vita rifiuta

Così preziosa come il vino, così gratis come la tristezza...

E il naufragar m'è dolce in questo mare.
`

describe(`Caesar's cipher`, () => {
    test('plain texts remains unchanged with key 0', () => {
        expect(caesar(plainText, 0)).toBe(plainText.toLowerCase());
    })

    test('can encrypt and decript using keys with sum 0', () => {
        const key = 11
        const encrypted = caesar(plainText,key)
        const decrypted = caesar(encrypted, 26 - key)
        expect(decrypted).toBe(plainText.toLowerCase())
    })
})
